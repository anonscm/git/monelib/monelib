/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.univtours.smartcard.pcsc;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Cette classe, au moment de l'instanciation, démarre un thread qui va scruter le lecteur et détecter toute insertion ou retrait de carte et
 * appeler respectivement les méthodes insertedCard() et removedCard() des objets qui implémentent l'interface CardEventListener et qui sont
 * inscrits dans les listes d'observateurs.
 *
 * @see CardEventListener
 * 
 * 
 */
public class Scrutation {

    private volatile Thread t;
    private ThreadScrute scrute;
    private ArrayList<CardEventListener> listeObservateurInsertCarte = new ArrayList<CardEventListener>();
    private ArrayList<CardEventListener> listeObservateurRetraitCarte = new ArrayList<CardEventListener>();


    /**
     * Démarre un thread qui va scruter les retraits et insertions sur le lecteur dont le nom est passé passé en paramètre
     * @param readerName Represente le nom du lecteur PCSC sur lequel on désire activer la scrutation
     *
     */

    public Scrutation(String readerName){
        scruteReader(readerName);
    }

    /**
     *
     * Cette méthode peut être appelée à chaque fois qu'on veux changer le lecteur scruté par le thread.
     * @param readerName Represente le nom du lecteur PCSC sur lequel on désire activer la scrutation
     *
     */

    public final void scruteReader(String readerName){
        
        // si le thread est en cours
        if (t != null && !t.isInterrupted()){
             Thread moribund = t;
             t = null;
             moribund.interrupt();
        }
        scrute = new ThreadScrute(readerName);
        scrute.setListeObservateurInsertCarte(listeObservateurInsertCarte);
        scrute.setListeObservateurRetraitCarte(listeObservateurRetraitCarte);

        t = new Thread(scrute);
        t.start();            
    }

    /**
     *
     * Cette méthode <b>abonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>insertion</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void addObserverInsertCarte(CardEventListener observer) {
            listeObservateurInsertCarte.add(observer);
            scrute.setListeObservateurInsertCarte(listeObservateurInsertCarte);
    }

    /**
     *
     * Cette méthode <b>désabonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>insertion</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void removeObserverInsertCarte(CardEventListener observer) {
            listeObservateurInsertCarte.remove(observer);
            scrute.setListeObservateurInsertCarte(listeObservateurInsertCarte);
    }

    /**
     *
     * Cette méthode <b>abonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>retrait</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void addObserverRetraitCarte(CardEventListener observer) {
            listeObservateurRetraitCarte.add(observer);
            scrute.setListeObservateurRetraitCarte(listeObservateurRetraitCarte);
    }

    /**
     *
     * Cette méthode <b>désabonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>retrait</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void removeObserverRetraitCarte(CardEventListener observer) {
            listeObservateurRetraitCarte.remove(observer);
            scrute.setListeObservateurRetraitCarte(listeObservateurRetraitCarte);
    }


}
