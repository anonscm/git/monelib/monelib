package fr.univtours.smartcard.pcsc;

import fr.univtours.smartcard.Exceptions.PCSCException;
import javax.smartcardio.Card;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;


/**
 *  Represente la connexion à la carte à puce
 */

public class ConnectionCard {
	

	private CardTerminal myReader;
	private Card card;
        private CardChannel channel;

	/**
         *  Etabli la connexion à la carte depuis le lecteur identifié en paramètre.
         *  @param readerName Represente le nom du lecteur PCSC sur lequel on désire se connecter
         *  @see PcscReader#getPCSCReaders() 
         *
         */
	public ConnectionCard(String readerName) throws PCSCException {
            CardTerminal reader = PcscReader.getCardTerminal(readerName);
            this.myReader = reader;
            if (card == null){
                try {
                    card = myReader.connect("*");
                } catch (CardException ex) {
                    throw new PCSCException(ex.getMessage());
                }
            }
	}
        
	/**
         *  Retourne le "logical channel" utilisé pour les echanges APDU
         */
        CardChannel getChannel(){
            if (channel == null){
                channel = card.getBasicChannel();
            }
            return channel;

        }

        /**
         *  Retourne l'objet carte obtenu à la connexion au lecteur
         */
	Card getCard(){
		return this.card;
	}

         /**
         *  Deconnecte la carte, elle ne peut plus être lue
         */
	public void closeCard() throws PCSCException {
            try {
                card.disconnect(true);
            } catch (CardException ex) {
                throw new PCSCException(ex.getMessage());
            }
            card = null;
	}
	


	

	
	
}
