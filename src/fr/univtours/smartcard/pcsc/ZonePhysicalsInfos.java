package fr.univtours.smartcard.pcsc;


import fr.univtours.smartcard.Exceptions.PCSCException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.ATR;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;





public class ZonePhysicalsInfos implements CardZone {

    private String uid;
     CardChannel channel;
     ConnectionCard connCard;
    //private static Apdu.Format format = new Apdu.Format(Apdu.HEX_VOID_HEX_FORMAT, false, false);

    public ZonePhysicalsInfos(ConnectionCard connCard) throws PCSCException {
        this.connCard = connCard;
        channel = connCard.getChannel();
        if (channel != null){
            uid = this.getUidFromCard();
        }
    }

    private String getUidFromCard(){
        CommandAPDU request = null;
        byte[] response = null;

        request = new CommandAPDU(Util.s2ba(APDUCommands.GET_UID));
        try {
            response = channel.transmit(request).getBytes();
        } catch (CardException ex) {
            Logger.getLogger(ZonePhysicalsInfos.class.getName()).log(Level.SEVERE, null, ex);
        }

    if (Util.isOK(response)){
            uid = Util.ba2s(response);
    }
    if (uid != null){
            // on enleve les 4 derniers caract�re = 9000 pour r�ussite
            uid = uid.substring(0, (uid.length()-4));
    }
    return uid;
    }


    public String getUid() {
            return uid;
    }

    public String getATR(){
         ATR myAtr = this.connCard.getCard().getATR();
         return Util.ba2s(myAtr.getBytes());
    }

    public String getTypeCard(){

            String typeCard = this.getATR();
            String literalTypeCard = "";

            if ("3B89800180670412B003020100049".equals(typeCard)){
                    literalTypeCard = "ISO 14443 - A";
            }else if ("3B888001000000007381930068".equals(typeCard)){
                    literalTypeCard = "ISO 14443 - B";
            }else if ("3B8F8001804F0CA0000003060300020000000069".equals(typeCard)){
                    literalTypeCard = "MIFARE 4K";
            }else if ("3B8F8001804F0CA0000003060300030000000068".equals(typeCard)){
                    literalTypeCard = "MIFARE Ultralight";
            }else if ("38828001024445".equals(typeCard)){
                    literalTypeCard = "NFC Forum tag type 1";
            }else if ("388C80010443FD0114E4007E0AC0319350".equals(typeCard)){
                    literalTypeCard = "NFC Forum tag type 3";
            }else if ("3B888001210000010081B12039".equals(typeCard)){
                    literalTypeCard = "Moneo BMS2 (sans contact)";
            }else if ("3B66000090D1020110B1".equals(typeCard)){
                    literalTypeCard = "Moneo BMS1 (contact)";
            }else{
                    literalTypeCard = "Inconnu";
            }


            return literalTypeCard;
    }

    public void printZone() {
         System.out.println("--------- Infos carte ---------");
         System.out.println("pupi : " + uid);
         System.out.println("type de carte : " + getTypeCard());
         System.out.println("ATR : " + getATR());
         System.out.println("");
    }
	
	
	
}
