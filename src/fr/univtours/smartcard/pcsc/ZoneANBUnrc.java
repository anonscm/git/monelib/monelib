/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univtours.smartcard.pcsc;

import fr.univtours.smartcard.Exceptions.PCSCException;
import fr.univtours.smartcard.pcsc.ConnectionCard;
import fr.univtours.smartcard.pcsc.Util;
import fr.univtours.smartcard.pcsc.ZoneANB;

/**
 *
 * @author geoffroy.vibrac
 */
public abstract class ZoneANBUnrc extends ZoneANB {
    
        public ZoneANBUnrc(ConnectionCard connCard, int ZoneAnbCrousRecherchee) throws PCSCException {

            super(connCard);
            byte[] zoneData = null;

            for (int i=0; i<10;i++){
                zoneData = getZoneContent(i);
                if (compareIdCrous(zoneData, ZoneAnbCrousRecherchee)){
                    this.zoneDump = zoneData;
                    break;
                }
            }
            if (zoneData == null){
                throw new PCSCException("Impossible de trouver la zone ANB " + ZoneAnbCrousRecherchee);
            }
        }
   
    final boolean compareIdCrous(byte[] contentZone, int numeroZoneCrous){

        // pour une zone crous, doit être = 0010
        String identifiantCnous="";
        // 1 = zone principale  - 2 = zone complementaire
        String identifiantBloc="";
        boolean returnValue = false;

        String contentZoneS = Util.ba2s(contentZone);

        if (contentZoneS!= null && !"".equals(contentZoneS) && contentZoneS.length() > 120){
            identifiantCnous = contentZoneS.substring(0, 4);
            identifiantBloc = contentZoneS.substring(8, 10);
            try{
                int identifiantBlocI = Integer.parseInt(identifiantBloc);
                if ("001A".equals(identifiantCnous) && identifiantBlocI == numeroZoneCrous){
                    returnValue = true;
                }
            }catch(ClassCastException e){  
            }
        }


        return returnValue;
    }


    int dateFormat(byte bOctet1, byte bOctet2){

            String octet1 = Util.getHexString(bOctet1);
            String octet2 = Util.getHexString(bOctet2);

            int annee = (Integer.parseInt(octet1,16) >> 1) + 2000;
            int mois = ((Integer.parseInt(octet1,16) & 0x01) << 3) | ((Integer.parseInt(octet2,16) & 0xe0) >> 5);
            int jour = (Integer.parseInt(octet2,16) & 0x1f);

            String laDate = annee + "" + mois + "" + jour;

            return Integer.parseInt(laDate);
    }

    abstract void constuctCard()  throws PCSCException;
    
    public abstract void printZone();    
}
