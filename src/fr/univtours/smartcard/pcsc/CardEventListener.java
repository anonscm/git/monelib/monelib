package fr.univtours.smartcard.pcsc;

    /**
     *
     *  L'interface CardEventListener doit être implémentée par toutes les classes
     *  qui désirent écouter les insertions et retraits de carte du lecteur.
     *  @see Scrutation
     *
     */

public interface CardEventListener {

    /**
     *
     *  Lorsqu'un objet implémente cette interface et est ajouté à la liste des observateurs, cette méthode est appelée à chaque insertion de carte
     *  @see Scrutation#addObserverInsertCarte(CardEventListener o)
     *  @see Scrutation#removeObserverInsertCarte(CardEventListener o)
     *
     */
     public void  insertedCard();

    /**
     *
     *  Lorsqu'un objet implémente cette interface et est ajouté à la liste des observateurs, cette méthode est appelée à chaque retrait de carte
     *  @see Scrutation#addObserverRetraitCarte(CardEventListener o)
     *  @see Scrutation#removeObserverRetraitCarte(CardEventListener o)
     *
     */
     public void  removedCard();
}