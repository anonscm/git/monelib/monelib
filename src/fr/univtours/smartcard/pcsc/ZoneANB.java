package fr.univtours.smartcard.pcsc;

import fr.univtours.smartcard.Exceptions.PCSCException;
import java.util.HashSet;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CommandAPDU;


/**
 *
 * Cette classe permet l'accès aux Zones ANB de la carte. Elle propose un ensemble de méthodes pour accéder et lire le contenu brut des zones.
 * Pour accéder au contenu formaté des zones dont le mapping est connu, il est conseillé d'instancier les sous classes
 *
 * @see ZoneANBCrousPrincipale
 * @see ZoneANBCrousComplementaire
 *
 */

public class ZoneANB{
        
    byte[] zoneDump;
    ConnectionCard connCard;


    /**
     *
     * Construit l'objet à partir d'un objet ConnectionCard
     *
     * @param connCard connexion valide à la carte
     * @see ConnectionCard
     *
     */
    public ZoneANB(ConnectionCard connCard) throws PCSCException {
        this.connCard = connCard;
    }

    /**
     *
     * Affiche vers la sortie le contenu brut des 10 zones ANB de la carte
     *
     */
    public final void printAllZonesDump() throws PCSCException{
        for (int i=0;i<10;i++){
            System.out.println("");
            System.out.println("--------- ANB "+(i+1)+" ---------");
            System.out.println(Util.ba2s(getZoneContent(i)));
        }
    }

     /**
     *
     * Retourne un Hashset des chaines représentant les  10 zones ANB de la carte
     *
     */
    public final HashSet<String> getAllZonesDump() throws PCSCException{
        HashSet<String> zonesDump= new HashSet<String>();
        for (int i=0;i<10;i++){
            zonesDump.add(Util.ba2s(getZoneContent(i)));
        }
        return zonesDump;
    }

    /**
     *
     * Affiche vers la sortie le contenu de la zone ANB indiquées en paramètre
     * @param numZoneCard numéro de la zone recherchée (entre 1 et 10)
     *
     */
    public final void printZoneDump(int numZoneCard) throws PCSCException {
        // on enleve 1 car les commandes APDU pour accéder à la zone 1 sont dans le tableau 0
        numZoneCard--;
        System.out.println("--------- ANB "+numZoneCard+"---------");
        System.out.println(Util.ba2s(getZoneContent(numZoneCard)));
    }

    /**
     *
     * Affiche vers la sortie le contenu de la zone ANB correspondant à l'objet instancié
     * Utile pour les classes filles
     *
     */
    public void printZoneDump() throws PCSCException {
        System.out.println("--------- ANB ---------");
        System.out.println(Util.ba2s(this.zoneDump));
    }

     /**
     *
     * Retourne une chaine représentant le contenu de la zones ANB indiquée en paramètre
     * @param numZoneCard numéro de la zone recherchée (entre 1 et 10)
     * 
     */
    public final String getZoneDump(int numZoneCard) throws PCSCException {
         // on enleve 1 car les commandes APDU pour accéder à la zone 1 sont dans le tableau 0
        numZoneCard--;
        return Util.ba2s(getZoneContent(numZoneCard));
    }


    byte[] getZoneContent(int numZoneCard) throws PCSCException {

        CommandAPDU request = null;
        byte[] zone = null;
        byte[] response = null;
        CardChannel channel = connCard.getChannel();

        if (channel !=null) {
            for (int j = 0; j < APDUCommands.ASK_ALL[numZoneCard].length; j++) {
                try {
                    request = new CommandAPDU(Util.s2ba(APDUCommands.ASK_ALL[numZoneCard][j]));
                    response = channel.transmit(request).getBytes();
                    // si on est à la dernière chaine APDU du tableau => la réponse correspond aux données
                    if (j == APDUCommands.ASK_ALL[numZoneCard].length - 1) {

                         zone = response;
                    }
                } catch (Exception e) {
                    throw new PCSCException(e.getMessage());
                }
            }
        }
        return zone;

    }

}
