package fr.univtours.smartcard.pcsc;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;




class ThreadScrute extends Thread {

    private ArrayList<CardEventListener> listeObservateurInsertCarte = new ArrayList<CardEventListener>();
    private ArrayList<CardEventListener> listeObservateurRetraitCarte = new ArrayList<CardEventListener>();
    private String reader = null;
    private CardTerminal myReader = null;
    private boolean scrute = true;


    public ThreadScrute(String reader){
            this.reader = reader;
            myReader = PcscReader.getCardTerminal(reader);
    }

    @Override
    public void run(){
            lanceScrutation();
    }

	
    void lanceScrutation(){


            while(scrute == true){
                try {
                    if (!myReader.isCardPresent()){
                        myReader.waitForCardPresent(0);
                        this.notifyObserverInsertCarte();
                    }else{
                         myReader.waitForCardAbsent(0);
                         this.notifyObserverRetraitCarte();
                    }
                } catch (CardException ex) {
                    Logger.getLogger(ThreadScrute.class.getName()).log(Level.SEVERE, null, ex);
                }
            }	
    }

    private void notifyObserverInsertCarte() {

            for (int i =0; i<listeObservateurInsertCarte.size();i++){
                    CardEventListener obs = listeObservateurInsertCarte.get(i);
                    obs.insertedCard();
            }
    }

    private void notifyObserverRetraitCarte() {

            for (int i =0; i<listeObservateurRetraitCarte.size();i++){
                    CardEventListener obs = listeObservateurRetraitCarte.get(i);
                    obs.removedCard();
            }
    }

    ArrayList<CardEventListener> getListeObservateurInsertCarte() {
        return listeObservateurInsertCarte;
    }

    void setListeObservateurInsertCarte(ArrayList<CardEventListener> listeObservateurInsertCarte) {
        this.listeObservateurInsertCarte = listeObservateurInsertCarte;
    }

    ArrayList<CardEventListener> getListeObservateurRetraitCarte() {
        return listeObservateurRetraitCarte;
    }

    void setListeObservateurRetraitCarte(ArrayList<CardEventListener> listeObservateurRetraitCarte) {
        this.listeObservateurRetraitCarte = listeObservateurRetraitCarte;
    }
	
	

	
}

