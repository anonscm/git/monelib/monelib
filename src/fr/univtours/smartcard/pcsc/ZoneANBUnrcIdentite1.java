/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.univtours.smartcard.pcsc;

import fr.univtours.smartcard.Exceptions.PCSCException;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author geoffroy.vibrac
 */
public class ZoneANBUnrcIdentite1 extends ZoneANBUnrc {
    private String numeroIdentifiant;

    /**
     *
     * Construit l'objet ZoneANBUnrcIdentite1 à partir d'un objet ConnectionCard et charge les informations lues sur la carte
     *
     * @param connCard connexion valide à la carte
     * @see ConnectionCard
     *
     */
    public ZoneANBUnrcIdentite1(ConnectionCard connCard) throws PCSCException {

        super(connCard, 2);
        constuctCard();

    }


    final void constuctCard() throws PCSCException{
        if (this.zoneDump!= null){
            constructNumeroIdentifiant();
        }
    }

    /**
     *
     * Affiche vers la sortie le contenu détaillé de la zone ANB
     *
     */
    public void printZone(){
        System.out.println("--------- ANB UNRC IDENTITE 1 ---------");
        System.out.println("Numero Carte : " + numeroIdentifiant);
        System.out.println("");
    }


    private void constructNumeroIdentifiant() throws PCSCException {
            byte[] bTemp = new byte[8];
            System.arraycopy(zoneDump, 8, bTemp, 0, 8);
            
            System.out.println(Util.ba2s(bTemp));
            try {
                    this.numeroIdentifiant = new String(bTemp, "ISO-8859-1");
            } catch (UnsupportedEncodingException e) {
                    throw new PCSCException(e.getMessage());
            } 
    }

    public String getNumeroIdentifiant() {
        return numeroIdentifiant;
    }
        
        
    
}
