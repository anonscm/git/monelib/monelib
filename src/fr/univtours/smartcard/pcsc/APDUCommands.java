package fr.univtours.smartcard.pcsc;

abstract class APDUCommands {

	public static String[] ASK_ZONE_1 = {"00A4040C06A00000006991", "00B201BC16", "00b201C43C"};
	public static String[] ASK_ZONE_2 = {"00A4040C06A00000006991", "00B201BC16", "00b202C43C"};
	public static String[] ASK_ZONE_3 = {"00A4040C06A00000006991", "00B201BC16", "00b203C43C"};
	public static String[] ASK_ZONE_4 = {"00A4040C06A00000006991", "00B201BC16", "00b204C43C"};
	public static String[] ASK_ZONE_5 = {"00A4040C06A00000006991", "00B201BC16", "00b205C43C"};
	public static String[] ASK_ZONE_6 = {"00A4040C06A00000006991", "00B201BC16", "00b206C43C"};
	public static String[] ASK_ZONE_7 = {"00A4040C06A00000006991", "00B201BC16", "00b207C43C"};
	public static String[] ASK_ZONE_8 = {"00A4040C06A00000006991", "00B201BC16", "00b208C43C"};
	public static String[] ASK_ZONE_9 = {"00A4040C06A00000006991", "00B201BC16", "00b209C43C"};
	public static String[] ASK_ZONE_10 ={"00A4040C06A00000006991", "00B201BC16", "00b20AC43C"};
	
	public static String[][] ASK_ALL = {ASK_ZONE_1, ASK_ZONE_2, ASK_ZONE_3, ASK_ZONE_4, ASK_ZONE_5,
                                            ASK_ZONE_6, ASK_ZONE_7, ASK_ZONE_8, ASK_ZONE_9, ASK_ZONE_10};

	public static String SELECT_MONEO = "00A4040C06A00000006900";

	public static String SELECT_NUM_AND_DATE = "00B201BC16";

	public static String[] SOLDE_MONEO = {"00B201C409", "00B201440E"};

	public static String[] HISTO_PAIEMENTS = {"00B2","EC33","5","29"}; // La commande complete est HISTO_PAIEMENTS[0]+numenregistrement+HISTO_PAIEMENTS[1]
	
	public static String[] HISTO_RECHARGEMENT = {"00B2","E430","4","24"}; //2 dernier champ : Index montant et index date
	
	public static String GET_UID = "FFCA000000";

	
	
}
