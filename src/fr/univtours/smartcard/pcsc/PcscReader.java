/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.univtours.smartcard.pcsc;

import fr.univtours.smartcard.Exceptions.PCSCException;
import java.util.ArrayList;
import java.util.List;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.TerminalFactory;

/**
 * Cette classe contient les méthodes statiques pour accéder à la liste des lecteurs PCSC
 * @author geoffroy.vibrac
 */
public abstract class PcscReader {


    private PcscReader(){
        
    }

    /**
     * Retourne la liste des noms des lecteurs PCSC disponibles
     * @return une List de chaines
     */
    public static List<String> getPCSCReaders() throws PCSCException {

        List<String> readerString = null;
        TerminalFactory factory = TerminalFactory.getDefault();
        CardTerminals terminalList = factory.terminals();
        
        try {
            List<CardTerminal> readers = terminalList.list();
            if (readers.size() > 0 ){
                readerString = new ArrayList<String>();
                for(CardTerminal reader : readers){
                    readerString.add(reader.getName());
                }
            }

        } catch (CardException ex) {
            throw new PCSCException(ex.getMessage());
        }

        return readerString;
    }

     /**
     * Retourne l'objet CardTerminal correspondant au lecteur indiqué en paramètres
     * utilisé par Connectioncard pour se connecté à la carte
     *
     * @return un objet CardTerminal representant le lecteur
     */

    static CardTerminal getCardTerminal(String ReaderName){
        CardTerminals terminalList;
        TerminalFactory factory = TerminalFactory.getDefault();
        terminalList = factory.terminals();
        return terminalList.getTerminal(ReaderName);

    }


}
