package fr.univtours.smartcard.pcsc;

import fr.univtours.smartcard.Exceptions.PCSCException;
import java.io.UnsupportedEncodingException;
import javax.smartcardio.CardChannel;


/**
 *
 * Cette classe permet l'accès à la zone ANB principale utilisée par les CROUS.
 * Elle propose un ensemble de méthodes pour accéder au contenu de chaque informations stockée sur la carte :
 * - Nom et prénom
 * - Numero de carte CROUS
 * - Numero d'affectation
 * - Code Société
 * - Code RNE
 * - Code tarif
 * - Date de fin de validité
 * - Code Société Nationale
 *
 *
 * @see ZoneANBCrous
 * @see ZoneANBCrousComplementaire
 *
 */
public class ZoneANBCrousPrincipale extends ZoneANBCrous {
	private String nom;
	private long numeroCarte;
	private short affectation;
	private long codeSociete;
	private String rne;
	private int tarif;
	private int dateFinValidite;
	private int codeSocieteNationale;

        /**
         *
         * Construit l'objet ZoneANBCrousPrincipale à partir d'un objet ConnectionCard et charge les informations lues sur la carte
         *
         * @param connCard connexion valide à la carte
         * @see ConnectionCard
         *
         */
        public ZoneANBCrousPrincipale(ConnectionCard connCard) throws PCSCException {

            super(connCard, 1);
            constuctCard();

        }

	
	final void constuctCard() throws PCSCException{
            if (this.zoneDump!= null){
		constructNumeroCarte();
		constructNom();
		constructAffectation();
		constructCodeSociete();
		constructTarif();
		constructDateFinValidite();
		constructCodeSocieteNationale();
		constructRne();
            }
	}

        /**
         *
         * Affiche vers la sortie le contenu détaillé de la zone ANB
         *
         */
	public void printZone(){
            System.out.println("--------- ANB CROUS 1 ---------");
            System.out.println("Nom : " + nom);
            System.out.println("numero Carte : " + numeroCarte);
            System.out.println("affectation : " + affectation);
            System.out.println("Code Societe : " + codeSociete);
            System.out.println("Tarif : " + tarif);
            System.out.println("Date fin validite : " + Util.afficheDate(dateFinValidite));
            System.out.println("Rne : " + rne);
            System.out.println("");
	}

	
	private void constructNom() throws PCSCException {
		byte[] bTemp = new byte[30];
		System.arraycopy(zoneDump, 7, bTemp, 0, 30);
		try {
			this.nom = new String(bTemp, "ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
			throw new PCSCException(e.getMessage());
		} 
	}
	
	private void constructNumeroCarte(){


		byte[] bTemp = new byte[4];
		System.arraycopy(zoneDump, 37, bTemp, 0, 4);
		// getHexString renvoye la zone en hexa
		// Integer.parseInt(x,16) converti l'hexa en décimal
		this.numeroCarte = Integer.parseInt(Util.getHexString(bTemp),16);	

		// a essayer :
		// this.numeroCarte = (zoneDump[40]& 0xFF) <<24 | (zoneDump[39]& 0xFF) <<16 | (zoneDump[38]& 0xFF) <<8 | (zoneDump[37] & 0xFF);

		// long val = (((long)b[0] & 0xFF) << 0) + (((long)b[1] & 0xFF) << 8) + (((long)b[2] & 0xFF) << 16) + (((long)b[3] & 0xFF) << 24);

	}
	
	private void constructAffectation(){
		this.affectation = Short.parseShort(Util.getHexString(zoneDump[41]),16);		
	}

	private void constructCodeSociete(){
		byte[] bTemp = new byte[2];
		System.arraycopy(zoneDump, 42, bTemp, 0, 2);
		this.codeSociete = Integer.parseInt(Util.getHexString(bTemp),16);		
	}
	
	private void constructRne() throws PCSCException{
		int octet1 = Integer.parseInt(Util.getHexString(zoneDump[44]),16);
		int octet2 = Integer.parseInt(Util.getHexString(zoneDump[45]),16);
		int octet3 = Integer.parseInt(Util.getHexString(zoneDump[46]),16);
		int octet4 = Integer.parseInt(Util.getHexString(zoneDump[47]),16);
		byte[] b = {zoneDump[48]};
		int octet5 = Integer.parseInt(Util.getHexString(zoneDump[48]),16);
		String CodeEtab = null;
		
		if ((octet1 & 0x80) != 0)   { // test le 8ème bit (le plus à gauche de l'octet)=> si 0 = privatif
                    int ce1 = (int)((int)octet4 | (int)((int)octet3 << 8) | (int)((int)octet2 << 16));
                    try {
                            CodeEtab = ce1 + "" + new String(b, "ISO-8859-1");
                    } catch (UnsupportedEncodingException e) {
                            throw new PCSCException(e.getMessage());
                    }
                }
                else
                {
                    int ce1 = (int)((int)octet5 | (int)((int)octet4 << 8) | (int)((int)octet3 << 16) | (int)((int)octet2 << 24)| (int)((int)octet1 << 32));
                    CodeEtab = ce1+"";
		}
		this.rne = CodeEtab;
	
	}	
		
    private void constructTarif(){
            byte[] bTemp = new byte[2];
            System.arraycopy(zoneDump, 49, bTemp, 0, 2);
            this.tarif = Integer.parseInt(Util.getHexString(bTemp),16);
    }

    private void constructDateFinValidite(){
            dateFinValidite = dateFormat(zoneDump[51], zoneDump[52]);
    }

    private void constructCodeSocieteNationale(){
            byte[] bTemp = new byte[2];
            System.arraycopy(zoneDump, 53, bTemp, 0, 2);
            this.codeSocieteNationale = Integer.parseInt(Util.getHexString(bTemp),16);
    }

    /**
     *
     * Renvoie l'affectation
     *
     */
    public short getAffectation() {
        return affectation;
    }

    /**
     *
     * Renvoie le code société
     *
     */
    public long getCodeSociete() {
        return codeSociete;
    }

    /**
     *
     * Renvoie le code société national
     *
     */
    public int getCodeSocieteNationale() {
        return codeSocieteNationale;
    }

    /**
     *
     * Renvoie la date de fin de validité tel que stocké sur la carte
     *
     */
    public int getDateFinValidite() {
        return dateFinValidite;
    }

    /**
     *
     * Renvoie la date de fin de validité au format français
     *
     */
    public String getDateFinValiditeFormatee(){
        if (dateFinValidite!=0){
            return Util.afficheDate(dateFinValidite);
        }else{
            return "";
        }
    }

    /**
     *
     * Renvoie le nom
     *
     */
    public String getNom() {
        return nom;
    }

    /**
     *
     * Renvoie le numéro de carte CROUS
     *
     */
    public long getNumeroCarte() {
        return numeroCarte;
    }

    /**
     *
     * Renvoie le RNE
     *
     */
    public String getRne() {
        return rne;
    }

    /**
     *
     * Renvoie le tarif
     *
     */
    public int getTarif() {
        return tarif;
    }

	
}
