package fr.univtours.smartcard.pcsc;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

    /**
     *
     *  Représente les historiques de chargement et de paiement du porte monaie Monéo
     *  Ces historiques contiennent la date et le montant de l'opération
     *  @see ZonePME
     *
     */

public class HistoriqueMoneo {

	private double montant;
	private Date dateOperation;

        /**
         *
         *  Constuit un objet HistoriqueMoneo
         *  @param montant
         *          Montant de l'opération
         *  @param dateOperation
         *          Date de l'opération
         */
	public HistoriqueMoneo(double montant, Date dateOperation){
		this.montant = montant;
		this.dateOperation = dateOperation;
	}

         /**
         *
          * Retourne le montant de l'opération
         *
         */
	public double getMontant() {
		return this.montant;
	}

         /**
         *
         * Retourne la date de l'opération
         *
         */
	public Date getDateOperation() {
		return this.dateOperation;
	}

         /**
         *
         * Retourne la date et l'heure de l'opération au format souhaité
         *  @param localeCountry format de date souhaité
         *
         */
	public String getFormatedDateOperation(Locale localeCountry){
    	DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, localeCountry); //Locale.FRANCE
		return df.format(this.dateOperation);
	}

         /**
         *
         * Retourne la date et l'heure de l'opération au format français
         *
         */
	public String getFrenchFormatedDateOperation(){
		return getFormatedDateOperation(Locale.FRANCE);
	}

         /**
         *
         * Retourne le montant sous forme de chaine
         *
         */
	public String getFormatedMontant() {
		Double dMontant = montant;
		return dMontant.toString();
	}
		
			
		
}
