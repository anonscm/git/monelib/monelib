/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package fr.univtours.smartcard.Exceptions;

import javax.smartcardio.CardException;

/**
 * Cette classe représente les exceptions qui peuvent être renvoyées en cas d'erreur dans la bibliothèque
 * @author geoffroy.vibrac
 */
public class PCSCException extends CardException {


    public PCSCException(String message) {
        super(message);
    }

    

}
