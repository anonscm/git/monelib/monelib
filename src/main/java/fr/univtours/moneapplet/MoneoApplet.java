/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.moneapplet;

import fr.univtours.monelib.Exceptions.PCSCException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JApplet;
import javax.swing.JTabbedPane;
import netscape.javascript.JSObject;



public class MoneoApplet extends JApplet {

	private static final long serialVersionUID = 1249052803342487334L;
        private JSObject jso;
        String serverpath = "";
	
        @Override
	public void init(){
            

            try{
                this.jso = JSObject.getWindow(this);  
                System.out.println("||||||||||||"+jso.toString());
            }catch(UnsatisfiedLinkError e){
                e.printStackTrace();
            }
            
            try{
                
                serverpath = getDocumentBase().getHost();
                int port = getDocumentBase().getPort();
                if (port>=0){
                    serverpath += ":"+port;
                }
                serverpath += getDocumentBase().getPath();
                Logger.getLogger(MoneoApplet.class.getName()).log(Level.INFO, ">>>>>>>>>"+serverpath+"<<<<<<<<");
            }catch(Exception ex){
                Logger.getLogger(MoneoApplet.class.getName()).log(Level.INFO, ex.getLocalizedMessage());
            }


            // catch de l'exception "pas de lecteur " de TabOption
            try {

//                if (jso!=null){              
//                    jso.call("chargementApplet",new Boolean[] {true});
//                }                
                
                TabOption option = new TabOption(jso, serverpath);
                
                this.setSize(430, 320);
                PanelApplet uid = new TabUid(jso);
                PanelApplet anb = new TabAnb(jso);
                PanelApplet moneo = new TabMoneo(jso);
                PanelApplet univ = new TabUniv(jso);

                option.setAnb(anb);
                option.setMoneo(moneo);
                option.setUid(uid);
                option.setUniv(univ);
                option.computeSet();

                JTabbedPane onglet = new JTabbedPane();
                onglet.addTab("Moneo", moneo.getPanel());
                onglet.addTab("Univ", univ.getPanel());
                onglet.addTab("Crous", anb.getPanel());
                onglet.addTab("Carte", uid.getPanel());
                onglet.addTab("Paramètres", option.getPanel());
                this.getContentPane().add(onglet);
                
                if (jso!=null){              
                    jso.call("chargementApplet",new Boolean[] {false});
                }                 

            } catch (PCSCException ex) {
                if (jso!=null){    
                    jso.call("chargementApplet",new Boolean[] {false});
                    jso.call("showErreur",new String[] {"Aucun lecteur trouvé"});
                }      
                Logger.getLogger(MoneoApplet.class.getName()).log(Level.SEVERE, "Aucun lecteur trouvé");
                   //JOptionPane.showMessageDialog(this, "Aucun lecteur trouvé");
            }
        }


}