/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.moneapplet;

import fr.univtours.monelib.pcsc.ConnectionCard;
import fr.univtours.monelib.pcsc.ZonePhysicalsInfos;
import javax.smartcardio.CardException;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import netscape.javascript.JSObject;


public class TabUid implements PanelApplet {

    private JLabel typeCardLabel;
    private JLabel typeCardValue;
    private JLabel uidLabel;
    private JLabel uidValue;
    private JLabel atrLabel;
    private JLabel atrValue;
    private JDesktopPane panelFond = new JDesktopPane();
    private JSObject jso;
    

    public TabUid(JSObject jso) {
        
            this.jso = jso;  

            typeCardLabel = new JLabel();
            typeCardLabel.setText("Type de Carte :");
            typeCardLabel.setBounds(22, 20, 100, 14);

            typeCardValue = new JLabel();
            typeCardValue.setText("");
            typeCardValue.setBounds(120, 20, 295, 14);

            uidLabel = new JLabel();
            uidLabel.setText("UID (pupi) :");
            uidLabel.setBounds(23, 50, 100, 14);

            uidValue = new JLabel();
            uidValue.setText("");
            uidValue.setBounds(120, 50, 203, 14);

            atrLabel = new JLabel();
            atrLabel.setText("ATR :");
            atrLabel.setBounds(23, 80, 100, 14);

            atrValue = new JLabel();
            atrValue.setText("");
            atrValue.setBounds(120, 80, 203, 14);

            
            panelFond.add(typeCardLabel, JLayeredPane.DEFAULT_LAYER);
            panelFond.add(typeCardValue, JLayeredPane.DEFAULT_LAYER);
            panelFond.add(uidLabel, JLayeredPane.DEFAULT_LAYER);
            panelFond.add(uidValue, JLayeredPane.DEFAULT_LAYER);
            panelFond.add(atrLabel, JLayeredPane.DEFAULT_LAYER);
            panelFond.add(atrValue, JLayeredPane.DEFAULT_LAYER);
    }




    @Override
    public JDesktopPane getPanel(){
            return panelFond;
    }

    @Override
    public void cleanPanel() {
        uidValue.setText("");
        typeCardValue.setText("");
        atrValue.setText("");
            if (jso!=null){              
                jso.call("cleanPhysicals",null);
            }        
    }

    @Override
    public void loadDataPanel(ConnectionCard conCard) throws CardException {
            ZonePhysicalsInfos cardInfo = new ZonePhysicalsInfos(conCard);
            uidValue.setText(cardInfo.getUid());
            typeCardValue.setText(cardInfo.getTypeCard());
            atrValue.setText(cardInfo.getATR());
            
            String uid = "";
            if (cardInfo.getUid()!= null){
                uid = cardInfo.getUid();
            }
              
            
            if (jso!=null){              
                jso.call("updatePhysicals",new String[] {uid, cardInfo.getTypeCard(), cardInfo.getATR()});
            }
    }

     public String getLogInfo(){
         return "\nuid : " + uidValue.getText() + "\nType de carte : " + typeCardValue.getText();
     }
    
}
