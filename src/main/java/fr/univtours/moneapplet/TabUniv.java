/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.moneapplet;


import fr.univtours.monelib.pcsc.ConnectionCard;
import fr.univtours.monelib.pcsc.ZoneANBUnrcIdentite1;
import javax.smartcardio.CardException;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JTextField;
import netscape.javascript.JSObject;

/**
 *
 * @author geoffroy.vibrac
 */
public class TabUniv implements PanelApplet {
 
    private JLabel labelNumeroEtudiant;
    private JTextField  fieldNumeroIdentifiant;
    private JSObject jso;

    private JDesktopPane panelFond = new JDesktopPane();

    public TabUniv(JSObject jso) {
        
            this.jso = jso;

            labelNumeroEtudiant = new JLabel();
            labelNumeroEtudiant.setText("Numero Etudiant");
            labelNumeroEtudiant.setBounds(10, 20, 110, 14);

            fieldNumeroIdentifiant = new JTextField();
            fieldNumeroIdentifiant.setText("");
            fieldNumeroIdentifiant.setBounds(130, 20, 203, 21);


            panelFond.add(fieldNumeroIdentifiant, JLayeredPane.DEFAULT_LAYER);
            panelFond.add(labelNumeroEtudiant, JLayeredPane.DEFAULT_LAYER);
            

    }



	
    @Override
    public JDesktopPane getPanel(){
            return panelFond;
    }

    @Override
    public void cleanPanel() {
        fieldNumeroIdentifiant.setText("");
        if (jso!=null){
            jso.call("cleanBiblio", null);
        }        
    }

    @Override
    public void loadDataPanel(ConnectionCard conCard) throws CardException  {
        ZoneANBUnrcIdentite1 zi1 = new ZoneANBUnrcIdentite1(conCard);

        fieldNumeroIdentifiant.setText(zi1.getNumeroIdentifiant());
        if (jso!=null){
            String numeroIdentifiant = "";
            if (zi1.getNumeroIdentifiant()!=null){
                numeroIdentifiant = zi1.getNumeroIdentifiant();
            }
            jso.call("updateBiblio", new String[]{numeroIdentifiant});
        } 
        
    }
    
     public String getLogInfo(){
         return "";
     }
    
}
