/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.moneapplet;




import fr.univtours.monelib.Exceptions.PCSCException;
import fr.univtours.monelib.pcsc.CardEventListener;
import fr.univtours.monelib.pcsc.ConnectionCard;
import fr.univtours.monelib.pcsc.PcscReader;
import fr.univtours.monelib.pcsc.Scrutation;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardException;
import javax.swing.*;
import netscape.javascript.JSObject;



public class TabOption implements CardEventListener {

    public JComboBox readerBox;
    private List<String> readerNames;
    private String selectedReader;
    private JDesktopPane panelFond = new JDesktopPane();

    private JCheckBox cbAnb;
    private JCheckBox cbMoneo;
    private JCheckBox cbUid;
    private JCheckBox cbUniv;
    private HashSet<PanelApplet> panels = new HashSet<PanelApplet>();
    PanelApplet uid;
    PanelApplet univ;
    PanelApplet moneo;
    PanelApplet anb;
    private JLabel message;
    private JButton btnSave;
    private ConnectionCard conCard;
    private JLabel version;
    ActionListener changeCB = new OnChangeComboBox();

    Scrutation scrute = null;
    private JSObject jso;
    private String serverpath;
    
              
    String postValue ="";

    public TabOption(JSObject jso, String serverpath) throws PCSCException {
        
                this.jso = jso;
                this.serverpath = serverpath;

                
                
                readerNames = PcscReader.getPCSCReaders();


                version = new JLabel();
                version.setText("1.0.0");
                version.setBounds(365, 261, 34, 14);
                version.setFont(new java.awt.Font("Tahoma",2,8));
            
                JPanel inputp = new JPanel();
		inputp.setLayout(new BoxLayout(inputp, BoxLayout.Y_AXIS));
		
                btnSave = new JButton("Sauver paramètres");
                btnSave.addActionListener(new SauveAction());
                btnSave.setBounds(145, 240, 150, 25);

		JPanel p = new JPanel();		
		p.setPreferredSize(new java.awt.Dimension(430,320));
		p.setLayout(null);

                inputp.add(p);

		JLabel l = new JLabel("Lecteurs :");
		l.setBounds(8, 22, 58, 14);
                
                message = new JLabel("",JLabel.CENTER);
                message.setForeground(Color.red);
                message.setBounds(0, 200, 430, 25);


                
		readerBox = new JComboBox(readerNames.toArray());
		readerBox.setBounds(72, 19, 251, 17);
		readerBox.addActionListener(changeCB);

                cbMoneo = new JCheckBox("Lire des données Monéo", true);
                cbMoneo.setBounds(10, 50, 251, 17);
                cbMoneo.addActionListener(new OnChangeMoneoCheckBox());
                
                cbUid = new JCheckBox("Lire le PUPI", true);
                cbUid.setBounds(10, 70, 251, 17);
                cbUid.addActionListener(new OnChangeUidCheckBox());

                cbAnb = new JCheckBox("Lire les informations CROUS", true);
                cbAnb.setBounds(10, 90, 251, 17);
                cbAnb.addActionListener(new OnChangeAnBCheckBox());
                
                cbUniv = new JCheckBox("Lire les informations UNRC", true);
                cbUniv.setBounds(10, 110, 251, 17);
                cbUniv.addActionListener(new OnChangeUnrcCheckBox());                
                
		p.add(l);
                p.add(readerBox);
                p.add(cbAnb);
                p.add(cbMoneo);
                p.add(cbUid);
                p.add(cbUniv);
                p.add(btnSave);
                p.add(message);
                p.add(version);

                //readParameters();

		panelFond.setLayout(new BorderLayout());
		panelFond.setOpaque(true);
                panelFond.add(inputp, BorderLayout.NORTH);


                scrute = new Scrutation();
                scrute.addObserverInsertCarte(this);
                scrute.addObserverRetraitCarte(this);



    }

         public void excutePost(String msg) {
            
        
            URL url;
            URLConnection con;             
            try {
              
              String urlParameters = "message=" + URLEncoder.encode(msg, "UTF-8");    
              
              if ("".equals(serverpath)){
                  serverpath = "localhost:8080";
              }
              
              url = new URL("http://"+serverpath+"resources/net.vibrac.atoutwebmv.persistance.entities.logactivity/");            
              

              byte[] parameterAsBytes = urlParameters.getBytes(); 

              con = url.openConnection(); 
              System.out.println(url.getPath());
              con.setDoOutput(true); 
              con.setDoInput(true); 
              con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
              con.setRequestProperty("Content=length", String.valueOf(parameterAsBytes.length)); 
              con.setRequestProperty("Content-Language", "en-US"); 
              con.setUseCaches (false);
              
              
                try {
                  AccessController.doPrivileged(new SendRequest(con, parameterAsBytes) );
                }
                catch (PrivilegedActionException ex) {
                   Logger.getLogger(MoneoApplet.class.getName()).log(Level.INFO, ex.getLocalizedMessage());
                }

             
            } catch (Exception ex) {
                 Logger.getLogger(MoneoApplet.class.getName()).log(Level.INFO, ex.getLocalizedMessage());
            } finally {
            }
    }       
        
    private class SendRequest implements PrivilegedExceptionAction {

        URLConnection con;
        byte[] parameterAsBytes;
        
        public SendRequest(URLConnection con, byte[] parameterAsBytes) {
            this.con = con;
            this.parameterAsBytes = parameterAsBytes;
        }

        @Override
        public Boolean run() {
            try {
                DataOutputStream oStream; 
                oStream = new DataOutputStream (con.getOutputStream ());
                oStream.write(parameterAsBytes); 
                  oStream.flush(); 

                  BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream())); 
                  in.close(); 
                  oStream.close();

              return Boolean.valueOf(true);
            } catch (IOException ex) {
                Logger.getLogger(MoneoApplet.class.getName()).log(Level.SEVERE, null, ex);
                return Boolean.valueOf(false);
            }
        }
        
        
    }     
    
    private void readParameters(){
        FileInputStream in = null;
        try {
            Properties prop = new Properties();
            in = new FileInputStream(confFile());
            prop.load(in);

            String l = prop.getProperty("lecteur");
            if (l!= null && !"".equals(l) && !l.equals((String)readerBox.getSelectedItem())){
                readerBox.removeActionListener(changeCB);
                readerBox.setSelectedItem(l);
                selectedReader=l;
                readerBox.addActionListener(changeCB);

            }

            String a = prop.getProperty("anb");
            String m = prop.getProperty("moneo");
            String u = prop.getProperty("uid");                      
            String un = prop.getProperty("univ"); 


            try{

                if (a!=null && Boolean.parseBoolean(a)==false){
                    cbAnb.setSelected(false);

                }

                if (m!=null && Boolean.parseBoolean(m)==false){
                    cbMoneo.setSelected(false);
                }

                if (u!=null && Boolean.parseBoolean(u)==false){
                    cbUid.setSelected(false);
                }
                
                if (un!=null && Boolean.parseBoolean(un)==false){
                    cbUniv.setSelected(false);
                }                

            }catch(ClassCastException e){
                
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
            } catch (NullPointerException ex) {
                Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }


    private File confFile() throws IOException{

      File f;
      f=new File("/configurator.conf");
      if(!f.exists()){
        f.createNewFile();
      }
      return f;
      
    }


    private void writeParameters(){

        FileOutputStream in = null;
        try {
            Properties prop = new Properties();
            in = new FileOutputStream(this.confFile());

            prop.setProperty("lecteur", (String) readerBox.getSelectedItem());
            prop.setProperty("anb", String.valueOf(cbAnb.isSelected()));
            prop.setProperty("moneo", String.valueOf(cbMoneo.isSelected()));
            prop.setProperty("uid", String.valueOf(cbUid.isSelected()));
            prop.setProperty("univ", String.valueOf(cbUniv.isSelected()));

            prop.store(in,"");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(TabOption.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }




    public void computeSet(){
        addOrRemovePanel(cbAnb.isSelected(), anb);
        addOrRemovePanel(cbMoneo.isSelected(), moneo);
        addOrRemovePanel(cbUid.isSelected(), uid);
        addOrRemovePanel(cbUniv.isSelected(), univ);
    }

    // enleve ou ajoute un panel en fonction de la valeur addPanel
    public void addOrRemovePanel(boolean addPanel, PanelApplet panel){
        if (addPanel){
            this.panels.add(panel);
        }else{
            panel.cleanPanel();
            this.panels.remove(panel);
        }
    }

    public void setAnb(PanelApplet anb) {
        this.anb = anb;
    }

    public void setMoneo(PanelApplet moneo) {
        this.moneo = moneo;
    }


    public void setUid(PanelApplet uid) {
        this.uid = uid;
    }
    
    public void setUniv(PanelApplet univ) {
        this.univ = univ;
    }    


    public final String getSelectedReader(){

            selectedReader = (String) readerBox.getSelectedItem();
            return (String) readerBox.getSelectedItem();
    }

    public JComboBox getReaderBox(){
            return this.readerBox;
    }


    public JDesktopPane getPanel() {
        return this.panelFond;
    }

    @Override
    public void insertedCard(String readerName) {

                message.setText("");
                double diff = 0;
                Calendar startTime = Calendar.getInstance();
                try{
                    if (jso!=null){
                        jso.call("viewWaitMessage", null);
                    }
                }catch(Exception e){
                    
                }
                //btnLectureCarte.setEnabled(false);
                postValue ="Debut Lecture";
                
                try {
                    String ip = InetAddress.getLocalHost().getHostAddress ();
                    postValue +="\n IP : " + ip;
                    System.out.println(ip);
                } catch (Exception ex) {
                }
                

                try{
                    conCard = new ConnectionCard(readerName);

                    if (conCard != null && panels!=null && panels.size()>0){
                        for(PanelApplet pa : panels){
                            try {
                                pa.loadDataPanel(conCard);
                                postValue += pa.getLogInfo();
                            } catch (CardException ex) {
                                Logger.getLogger(TabMoneo.class.getName()).log(Level.SEVERE, null, ex);
                                postValue +="\n" + ex.getLocalizedMessage();
                            }
                        }

                    }
                }catch(PCSCException e){
                    if (jso!=null){    
                        jso.call("showErreur",new String[] {e.getMessage()});
                        jso.call("hideWaitMessage",null);
                        jso.call("hideWaitDuration",null);                        
                    }      
                    Logger.getLogger(MoneoApplet.class.getName()).log(Level.SEVERE, e.getMessage());                    
                    postValue +="\n" + e.getLocalizedMessage();
                }
                Calendar stopTime = Calendar.getInstance();

                diff = stopTime.getTime().getTime() - startTime.getTime().getTime();
                diff = diff/1000;
                postValue +="\nDurée de lecture : " + diff + " secondes";
                excutePost(postValue);

                if (diff > 0)
                 message.setText("Lecture en " + diff + " seconde(s)");
                
                if (jso!=null){
                    jso.call("hideWaitMessage", new String[] {diff+""});
                }                

    }

    @Override
    public void removedCard(String readerName) {
            message.setText("");
            for(PanelApplet pa : panels){
                 pa.cleanPanel();
            }
                if (jso!=null){
                    jso.call("hideWaitDuration", null);
                }             
    }
	
	
    class OnChangeComboBox implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent arg0) {
                    selectedReader = (String) readerBox.getSelectedItem();
                    scrute.scruteReader(selectedReader);

            }

    }

    class OnChangeAnBCheckBox implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                 addOrRemovePanel(cbAnb.isSelected(), anb);
            }

    }
    
    class OnChangeUnrcCheckBox implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                 addOrRemovePanel(cbUniv.isSelected(), univ);
            }

    }    
    

    class OnChangeMoneoCheckBox implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                 addOrRemovePanel(cbMoneo.isSelected(), moneo);
            }

    }

    class OnChangeUidCheckBox implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {
                 addOrRemovePanel(cbUid.isSelected(), uid);
            }

    }

    public class SauveAction implements ActionListener{

            @Override
            public void actionPerformed(ActionEvent e) {


                writeParameters();



            }
    }

}
