/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.monelib.pcsc;


import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;




class ThreadScrute extends Thread {

    private ArrayList<CardEventListener> listeObservateurInsertCarte = new ArrayList<CardEventListener>();
    private ArrayList<CardEventListener> listeObservateurRetraitCarte = new ArrayList<CardEventListener>();
    private String reader = null;
    private CardTerminal myReader = null;
    private boolean scrute = true;


    public ThreadScrute(String reader){
            this.reader = reader;
            myReader = PcscReader.getCardTerminal(reader);
    }

    @Override
    public void run(){        
       lanceScrutation();
    }

	
    void lanceScrutation(){


            while(scrute == true){
                try {
                    if (!myReader.isCardPresent()){
                        myReader.waitForCardPresent(0);
                        this.notifyObserverInsertCarte(myReader.getName());
                    }else{
                         myReader.waitForCardAbsent(0);
                         this.notifyObserverRetraitCarte(myReader.getName());
                    }
                } catch (CardException ex) {
                    Logger.getLogger(ThreadScrute.class.getName()).log(Level.SEVERE, null, ex);
                }catch(Exception e){
                    Logger.getLogger(">>>>>>>> Problème de carte");
                }
            }	
    }

    private void notifyObserverInsertCarte(String readerName) {

            for (int i =0; i<listeObservateurInsertCarte.size();i++){
                    CardEventListener obs = listeObservateurInsertCarte.get(i);
                    obs.insertedCard(readerName);
            }
    }

    private void notifyObserverRetraitCarte(String readerName) {

            for (int i =0; i<listeObservateurRetraitCarte.size();i++){
                    CardEventListener obs = listeObservateurRetraitCarte.get(i);
                    obs.removedCard(readerName);
            }
    }

    ArrayList<CardEventListener> getListeObservateurInsertCarte() {
        return listeObservateurInsertCarte;
    }

    void setListeObservateurInsertCarte(ArrayList<CardEventListener> listeObservateurInsertCarte) {
        this.listeObservateurInsertCarte = listeObservateurInsertCarte;
    }

    ArrayList<CardEventListener> getListeObservateurRetraitCarte() {
        return listeObservateurRetraitCarte;
    }

    void setListeObservateurRetraitCarte(ArrayList<CardEventListener> listeObservateurRetraitCarte) {
        this.listeObservateurRetraitCarte = listeObservateurRetraitCarte;
    }
	
	

	
}

