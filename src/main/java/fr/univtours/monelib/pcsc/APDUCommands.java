/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.monelib.pcsc;

abstract class APDUCommands {

	public static String[] ASK_ZONE_1 = {"00A4040C06A00000006991", "00B201BC16", "00b201C43C"};
	public static String[] ASK_ZONE_2 = {"00A4040C06A00000006991", "00B201BC16", "00b202C43C"};
	public static String[] ASK_ZONE_3 = {"00A4040C06A00000006991", "00B201BC16", "00b203C43C"};
	public static String[] ASK_ZONE_4 = {"00A4040C06A00000006991", "00B201BC16", "00b204C43C"};
	public static String[] ASK_ZONE_5 = {"00A4040C06A00000006991", "00B201BC16", "00b205C43C"};
	public static String[] ASK_ZONE_6 = {"00A4040C06A00000006991", "00B201BC16", "00b206C43C"};
	public static String[] ASK_ZONE_7 = {"00A4040C06A00000006991", "00B201BC16", "00b207C43C"};
	public static String[] ASK_ZONE_8 = {"00A4040C06A00000006991", "00B201BC16", "00b208C43C"};
	public static String[] ASK_ZONE_9 = {"00A4040C06A00000006991", "00B201BC16", "00b209C43C"};
	public static String[] ASK_ZONE_10 ={"00A4040C06A00000006991", "00B201BC16", "00b20AC43C"};
	
	public static String[][] ASK_ALL = {ASK_ZONE_1, ASK_ZONE_2, ASK_ZONE_3, ASK_ZONE_4, ASK_ZONE_5,
                                            ASK_ZONE_6, ASK_ZONE_7, ASK_ZONE_8, ASK_ZONE_9, ASK_ZONE_10};

	public static String SELECT_MONEO = "00A4040C06A00000006900";

	public static String SELECT_NUM_AND_DATE = "00B201BC16";

	public static String[] SOLDE_MONEO = {"00B201C409", "00B201440E"};

	public static String[] HISTO_PAIEMENTS = {"00B2","EC33","5","29"}; // La commande complete est HISTO_PAIEMENTS[0]+numenregistrement+HISTO_PAIEMENTS[1]
	
	public static String[] HISTO_RECHARGEMENT = {"00B2","E430","4","24"}; //2 dernier champ : Index montant et index date
	
	public static String GET_UID = "FFCA000000";

	
	
}
