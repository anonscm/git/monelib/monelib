/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.monelib.pcsc;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

    /**
     *
     *  Représente les historiques de chargement et de paiement du porte monaie Monéo
     *  Ces historiques contiennent la date et le montant de l'opération
     *  @see ZonePME
     *
     */

public class HistoriqueMoneo {

	private double montant;
	private Date dateOperation;

        /**
         *
         *  Constuit un objet HistoriqueMoneo
         *  @param montant
         *          Montant de l'opération
         *  @param dateOperation
         *          Date de l'opération
         */
	public HistoriqueMoneo(double montant, Date dateOperation){
		this.montant = montant;
		this.dateOperation = dateOperation;
	}

         /**
         *
          * Retourne le montant de l'opération
         *
         */
	public double getMontant() {
		return this.montant;
	}

         /**
         *
         * Retourne la date de l'opération
         *
         */
	public Date getDateOperation() {
		return this.dateOperation;
	}

         /**
         *
         * Retourne la date et l'heure de l'opération au format souhaité
         *  @param localeCountry format de date souhaité
         *
         */
	public String getFormatedDateOperation(Locale localeCountry){
    	DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, localeCountry); //Locale.FRANCE
		return df.format(this.dateOperation);
	}

         /**
         *
         * Retourne la date et l'heure de l'opération au format français
         *
         */
	public String getFrenchFormatedDateOperation(){
		return getFormatedDateOperation(Locale.FRANCE);
	}

         /**
         *
         * Retourne le montant sous forme de chaine
         *
         */
	public String getFormatedMontant() {
		Double dMontant = montant;
		return dMontant.toString();
	}
		
			
		
}
