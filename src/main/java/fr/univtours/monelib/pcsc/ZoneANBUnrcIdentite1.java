/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/


package fr.univtours.monelib.pcsc;

import fr.univtours.monelib.Exceptions.PCSCException;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author geoffroy.vibrac
 */
public class ZoneANBUnrcIdentite1 extends ZoneANBUnrc {
    private String numeroIdentifiant;

    /**
     *
     * Construit l'objet ZoneANBUnrcIdentite1 à partir d'un objet ConnectionCard et charge les informations lues sur la carte
     *
     * @param connCard connexion valide à la carte
     * @see ConnectionCard
     *
     */
    public ZoneANBUnrcIdentite1(ConnectionCard connCard) throws PCSCException {

        super(connCard, 2);
        constuctCard();

    }


    final void constuctCard() throws PCSCException{
        if (this.zoneDump!= null){
            constructNumeroIdentifiant();
        }
    }

    /**
     *
     * Affiche vers la sortie le contenu détaillé de la zone ANB
     *
     */
    public void printZone(){
        System.out.println("--------- ANB UNRC IDENTITE 1 ---------");
        System.out.println("Numero Carte : " + numeroIdentifiant);
        System.out.println("");
    }


    private void constructNumeroIdentifiant() throws PCSCException {
            byte[] bTemp = new byte[10];
            System.arraycopy(zoneDump, 25, bTemp, 0, 10);
            byte[] bTemp2 = stripBlanckBytes(bTemp);
            
            try {
                    this.numeroIdentifiant = new String(bTemp2, "ISO-8859-1");
            } catch (UnsupportedEncodingException e) {
                    throw new PCSCException(e.getMessage());
            } 
    }

    public String getNumeroIdentifiant() {
        return numeroIdentifiant;
    }
        
        
    private byte[] stripBlanckBytes(byte[] temp){
        int j=0;
        for (int i=0; i<temp.length;i++){
            if (temp[i]!=0){
                j++;
            }
        }
        byte[] bb = new byte[j];
        System.arraycopy(temp, 0, bb, 0, j);
        
        return bb;
    }
           
    
}
