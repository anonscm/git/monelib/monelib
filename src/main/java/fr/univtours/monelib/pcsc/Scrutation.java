/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.monelib.pcsc;

import fr.univtours.monelib.Exceptions.PCSCException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Cette classe, au moment de l'instanciation, démarre un thread qui va scruter le lecteur et détecter toute insertion ou retrait de carte et
 * appeler respectivement les méthodes insertedCard() et removedCard() des objets qui implémentent l'interface CardEventListener et qui sont
 * inscrits dans les listes d'observateurs.
 *
 * @see CardEventListener
 * 
 * 
 */
public class Scrutation extends Observable {

    private volatile Thread t;
    private List<ThreadScrute> scrute = new ArrayList<ThreadScrute>();
    private ArrayList<CardEventListener> listeObservateurInsertCarte = new ArrayList<CardEventListener>();
    private ArrayList<CardEventListener> listeObservateurRetraitCarte = new ArrayList<CardEventListener>();


    /**
     * Démarre un thread qui va scruter les retraits et insertions sur le lecteur dont le nom est passé passé en paramètre
     * @param readerName Represente le nom du lecteur PCSC sur lequel on désire activer la scrutation
     *
     */

    public Scrutation(String readerName){
        scruteReader(readerName);
    }
    
    public Scrutation(){
        try {
            for (String s : PcscReader.getPCSCReaders()){
                scruteReader(s);
            }
        } catch (PCSCException ex) {
            Logger.getLogger(Scrutation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * Cette méthode peut être appelée à chaque fois qu'on veux changer le lecteur scruté par le thread.
     * @param readerName Represente le nom du lecteur PCSC sur lequel on désire activer la scrutation
     *
     */

    public final void scruteReader(String readerName){
        
        // si le thread est en cours
        if (t != null && !t.isInterrupted()){
             Thread moribund = t;
             t = null;
             moribund.interrupt();
        }
        ThreadScrute myThread = new ThreadScrute(readerName);
        myThread.setListeObservateurInsertCarte(listeObservateurInsertCarte);
        myThread.setListeObservateurRetraitCarte(listeObservateurRetraitCarte);
        
        scrute.add(myThread);

        t = new Thread(myThread);
        t.start();            
    }

    /**
     *
     * Cette méthode <b>abonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>insertion</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void addObserverInsertCarte(CardEventListener observer) {
            listeObservateurInsertCarte.add(observer);
            for (ThreadScrute myThread : scrute){
                myThread.setListeObservateurInsertCarte(listeObservateurInsertCarte);
            }
    }

    /**
     *
     * Cette méthode <b>désabonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>insertion</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void removeObserverInsertCarte(CardEventListener observer) {
            listeObservateurInsertCarte.remove(observer);
            for (ThreadScrute myThread : scrute){
                myThread.setListeObservateurInsertCarte(listeObservateurInsertCarte);
            }                
    }

    /**
     *
     * Cette méthode <b>abonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>retrait</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void addObserverRetraitCarte(CardEventListener observer) {
            listeObservateurRetraitCarte.add(observer);
            for (ThreadScrute myThread : scrute){
                myThread.setListeObservateurRetraitCarte(listeObservateurRetraitCarte);
            }
    }

    /**
     *
     * Cette méthode <b>désabonne</b> un objet qui implémente l'interface CardEventListener à l'evenement <b>retrait</b> de carte
     * @param observer représente l'objet qui implémente CardEventListener.
     *
     */
    public void removeObserverRetraitCarte(CardEventListener observer) {
            listeObservateurRetraitCarte.remove(observer);
            for (ThreadScrute myThread : scrute){
                myThread.setListeObservateurRetraitCarte(listeObservateurRetraitCarte);
            }
    }


}
