/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.monelib.pcsc;

import fr.univtours.monelib.Exceptions.PCSCException;
import java.text.DateFormat;
import java.util.*;
import javax.smartcardio.CardChannel;
import javax.smartcardio.CardException;
import javax.smartcardio.CommandAPDU;

/**
 *  Represente la zone bancaire du porte monaie electronique
 */


public class ZonePME implements CardZone {

	private double solde;
	private ArrayList<HistoriqueMoneo> rechargements;
	private ArrayList<HistoriqueMoneo> payements;
        private CardChannel channel;
        private String serialNumber;
        private String expirationDate;

        /**
         *  Represente la zone bancaire du porte monaie electronique
         *  @throws CardException  Si jamais l'url est mal formée.
         */

	public ZonePME(ConnectionCard connCard) throws PCSCException{
                channel = connCard.getChannel();
		if (channel != null){
                    try {
                        if (this.selectMoneoApp()){
                            this.setSolde();
                            this.payements = reqHistorique(5, APDUCommands.HISTO_PAIEMENTS);
                            this.rechargements = reqHistorique(5, APDUCommands.HISTO_RECHARGEMENT);
                            this.setSerialNumberAndDate();
                        }
                    } catch (CardException ex) {
                        throw new PCSCException(ex.getMessage());
                    }
		}
	}



   private boolean selectMoneoApp() throws PCSCException{
        CommandAPDU request = null;
        byte[] response = null;
        boolean returnValue = false;

        try {

            request = new CommandAPDU(Util.s2ba(APDUCommands.SELECT_MONEO));
            response = channel.transmit(request).getBytes();
            if (Util.isOK(response)) {
                    returnValue = true;
            }
        } catch (CardException ex) {
            throw new PCSCException(ex.getMessage());
        }

        return returnValue;

   }


    // cette metode envoie la requete qui récupère le numéro de serie et la date de la carte
    private String setSerialNumberAndDate() throws PCSCException{

        CommandAPDU request = null;
        byte[] response = null;
        String returnValue = null;
        
        try {

                request = new CommandAPDU(Util.s2ba(APDUCommands.SELECT_NUM_AND_DATE));
                response = channel.transmit(request).getBytes();
                 if (Util.isOK(response)){
                    returnValue = Util.ba2s(response);
                    if (returnValue!=null){
                        this.serialNumber = returnValue.substring(0, 19);
                        this.expirationDate = returnValue.substring(22, 24) + "/" + returnValue.substring(20, 22);


                    }

             }
        } catch (CardException ex) {
            throw new PCSCException(ex.getMessage());
        }

        return returnValue;
    }



    private ArrayList<HistoriqueMoneo> reqHistorique(int numEnregistrement, String[] apduCommand) throws CardException {
        CommandAPDU request = null;
        byte[] response = null;
        String index;
        int montantIndex = Integer.parseInt(apduCommand[2]);
        int dateIndex = 0;
        ArrayList<HistoriqueMoneo> historique = new ArrayList<HistoriqueMoneo>();

        for (int i = 1; i <= numEnregistrement; i++) {
            index = Util.addZeroAGauche(i);
            request = new CommandAPDU(Util.s2ba(apduCommand[0] + index + apduCommand[1]));
            response = channel.transmit(request).getBytes();
            if (Util.isOK(response)) {
                if (response.length > 35) {
                    if ((response[24] == 0) && (response[25] == 0) && (response[26] == 0) && (response[27] == 0) && (response[28] == 0) && (response[29] == 0) && (response[30] == 0)) {
                    } else {
                        double montantOperationCourante = 0;
                        Date dateOperationCourante;
                        Calendar c = new GregorianCalendar();
                        for (int j = montantIndex; j < montantIndex + 3; j++) {
                           montantOperationCourante = (long) (montantOperationCourante * 100) + (long) (((response[j] >> 4) & 0xf) * 10) + (long) (response[j] & 0x0f);
                        }
                        montantOperationCourante /= 100;
                        dateIndex = Integer.parseInt(apduCommand[3]);
                        c.set((((response[dateIndex] & 0xf0) >> 4) * 1000) + ((response[dateIndex] & 0x0f) * 100) + (((response[++dateIndex] & 0xf0) >> 4) * 10) + (response[dateIndex] & 0x0f), (((response[++dateIndex] & 0xf0) >> 4) * 10) + (response[dateIndex] & 0x0f) - 1, (((response[++dateIndex] & 0xf0) >> 4) * 10) + (response[dateIndex] & 0x0f), (((response[++dateIndex] & 0xf0) >> 4) * 10) + (response[dateIndex] & 0x0f), (((response[++dateIndex] & 0xf0) >> 4) * 10) + (response[dateIndex] & 0x0f), (((response[++dateIndex] & 0xf0) >> 4) * 10) + (response[dateIndex] & 0x0f));
                        dateOperationCourante = c.getTime();
                        historique.add(new HistoriqueMoneo(montantOperationCourante, dateOperationCourante));
                    }
                }
            }
            
        }
        return historique;
    }
	
	private void setSolde() throws CardException{
            double solde=0;
            byte[] response;
            long bal = 0;
            long balRes = 0;
            CommandAPDU request = null;
    	

            request = new CommandAPDU(Util.s2ba(APDUCommands.SOLDE_MONEO[0]));
            response = channel.transmit(request).getBytes();

            if (Util.isOK(response)){

                for (int index = 0; index < 3; index++) {
                    bal = (long)(bal * 100) + (long)(((response[index] >> 4) & 0xf) * 10) + (long)(response[index] & 0x0f);
                }



                request = new CommandAPDU(Util.s2ba(APDUCommands.SOLDE_MONEO[1]));
                    response = channel.transmit(request).getBytes();

                if (Util.isOK(response)){

                    for (int index = 0; index < 3; index++) {
                            balRes = (long)(balRes * 100) + (long)(((response[index] >> 4) & 0xf) * 10) + (long)(response[index] & 0x0f);
                    }
                }else{
                    balRes = 0;
                }
                solde = (double)((bal + balRes) / 100.00);
            }

            this.solde = solde;
	}

	public double getSolde() {
		return solde;
	}

	public ArrayList<HistoriqueMoneo> getRechargements() {
		return rechargements;
	}

	public ArrayList<HistoriqueMoneo> getPayements() {
		return payements;
	}

        public String getExpirationDate() {
            return expirationDate;
        }

        public String getSerialNumber() {
            return serialNumber;
        }



        public static Object[][] getListHistoriqueToArray(ArrayList<HistoriqueMoneo> historique){
                if (historique == null){
                    return null;
                }


		int tabRange = historique.size();
		Object[][] objectTab = new String[tabRange][2];

		for (int i=0; i < tabRange; i++){
			objectTab[i][0] =  historique.get(i).getFormatedDateOperation(Locale.FRANCE);
			objectTab[i][1] =  historique.get(i).getFormatedMontant();
		}

		return objectTab;
	}
        
        public static String[][] getListHistoriqueToStringArrayWithNbsp(ArrayList<HistoriqueMoneo> historique){
                if (historique == null){
                    return null;
                }


		int tabRange = 5;
		String[][] objectTab = new String[tabRange][2];

		for (int i=0; i < tabRange; i++){
                        if ((i+1)>historique.size()){
                            objectTab[i][0] = "&nbsp;";
                            objectTab[i][1] = "&nbsp;";
                        }else{
                            objectTab[i][0] =  historique.get(i).getFormatedDateOperation(Locale.FRANCE);
                            objectTab[i][1] =  historique.get(i).getFormatedMontant();
                        }
		}

		return objectTab;
	}        

    public void printZone() {
                System.out.println("--------- MONEO ---------");
		System.out.println("--------- Solde ---------");
		System.out.println("Solde : " + this.solde);
                System.out.println("Numero Serie : " + this.serialNumber);
                System.out.println("Date expiration : " + this.expirationDate);
		System.out.println("--------- Payements ---------");
		for (HistoriqueMoneo h:this.payements){
        	DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.FRANCE);
			System.out.print("Date : " + df.format(h.getDateOperation()));
			System.out.println("  Montant : " + h.getMontant());
		}
		System.out.println("--------- Rechargements ---------");
		for (HistoriqueMoneo h:this.rechargements){
        	DateFormat df = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.FRANCE);
			System.out.print("Date : " + df.format(h.getDateOperation()));
			System.out.println("  Montant : " + h.getMontant());
		}
                System.out.println("");
    }




}
