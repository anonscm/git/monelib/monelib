/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.monelib.pcsc;


/**
 * Miscellaneous functions. 
 */
class Util{
    /** 
     * Read a line from standard in and trim it. 
     * @return the line read from standard in.
     */
    private static int MAX_BRACE_CNT = 10;

    private static final int HEX_SPACE_HEX_FORMAT = 0;
    /**
     * Output format: hex:hex:hex
     */
    private static final int HEX_COLON_HEX_FORMAT = 1;
    /**
     * Output format: hexhexhex
     */
    private static final int HEX_VOID_HEX_FORMAT = 2;

    private static String digits = "0123456789ABCDEF";



    static boolean isOK(byte[] myByteArray){

        if (myByteArray!=null){
            String apduTest = Util.ba2s(myByteArray);
            return apduTest.substring((apduTest.length()-4), apduTest.length()).equals("9000");
        }else{
            return false;
        }

    }

    static String getHexString(byte[] bytes) {
            StringBuilder sb = new StringBuilder(bytes.length*2);
            for (byte b : bytes) {
                    sb.append( String.format("%x", b) );
            }
            return sb.toString();
    }

    static byte[] s2ba(String s) {
        byte[] ba = new byte[s.length() * 2];
        int len = s2ba(s, ba, 0);
        byte[] ret = new byte[len];
        System.arraycopy(ba, 0, ret, 0, len);
        return ret;
    }


        public static int s2ba(String s, byte[] ba, int off) {
            int[] braceTab = new int[MAX_BRACE_CNT];
            int bracePos = -1;
            boolean asciiMode = false;
            int slen = s.length();
            String n;
            int bpos = off;
            int i = 0;
            byte b;

            while (i < slen) {
                char c = s.charAt(i);
                if (!asciiMode && (c == ':')){
                    i++;
                    continue;
                }
                if (!asciiMode && (c == ' ')){
                    while(s.charAt(++i) == ' ');
                    continue;
                }

                if (!asciiMode && (c == '#')){
                    ++i;
                    if (s.charAt(i) != '(')
                        throw new RuntimeException("miss ( after # in " + s);
                    if (bracePos + 1 >= MAX_BRACE_CNT)
                        throw new RuntimeException("too many ( in APDU spec " + s);
                    braceTab[++bracePos] = bpos++;
                    ++i;
                    continue;
                }
                if (!asciiMode && (c == ')')){
                    if (bracePos < 0)
                        throw new RuntimeException("invalid ) in APDU spec " + s);
                    ba[braceTab[bracePos]] = (byte)(bpos - braceTab[bracePos] - 1);
                    --bracePos;
                    ++i;
                    continue;
                }

                if (c == '|'){
                    asciiMode = !asciiMode;
                    i++;
                    continue;
                }

                if (asciiMode){
                    if (c >= 256)
                        throw new RuntimeException("invalid character in hex-string " + s);
                    b = (byte) ((int) c & 0xff);
                    ba[bpos++] = b;
                    i++;
                    continue;
                }

                try {
                    if ((c == '0') && ((s.charAt(i + 1) == 'x') || (s.charAt(i + 1) == 'X'))) {
                        i++;
                        c = s.charAt(i);
                    }

                    if ((c == 'x') || (c == 'X')) {
                        i++;
                        if ((s.length() == i + 1) || ((s.charAt(i + 1) == ':') || (s.charAt(i + 1) == ' '))){
                            n = s.substring(i, i + 1);
                            i++; // set i on :
                        } else {
                            n = s.substring(i, i + 2);
                            i += 2; // set i on :
                        }
                    } else {
                        n = s.substring(i, i + 2);
                        i += 2;
                    }

                    ba[bpos++] = (byte) (Integer.parseInt(n, 16) & 0xff);

                } catch (Exception e) {
                    throw new RuntimeException("invalid hexadecimal string " + s);
                }
            }

            if (bracePos >= 0)
                throw new RuntimeException("mismatched parentheses");

            return bpos - off;
        }


	public static String getHexString(byte b) {
		StringBuilder sb = new StringBuilder(2);
		sb.append( String.format("%x", b) );
		return sb.toString();
	}
       
	public static String afficheDate(int dateInt) {
		String dateS = String.valueOf(dateInt);
		return dateS.substring(6, 8) + "/" + dateS.substring(4, 6) + "/" + dateS.substring(0, 4) ;		
	}
	
	public static String addZeroAGauche(int i){
		String temp = i+"";
		if (temp.length() == 1){
			temp = "0"+temp;
		}
		return temp;
	}

    private static Format defaultFormat = new Format();

           /**
     * Return string representation for byte array.
     */
    public static String ba2s(byte[] ba){
	return ba2s(ba, 0, ba.length, defaultFormat);
    }

    /**
     * Return string representation for byte array range.
     */
    public static String ba2s(byte[] ba, int off, int len){
	return ba2s(ba, off, len, defaultFormat);
    }

    /**
     * Return string representation for byte array.
     */
    public static String ba2s(byte[] ba, Format f){
	return ba2s(ba, 0, ba.length, f);
    }

    /**
     * Return string representation for byte array range according to given format.
     */
    public static String ba2s(byte[] ba, int off, int len, Format f){
	StringBuffer sb = new StringBuffer();
	if (f.printLength){
	    sb.append(len).append(": ");
	}
	for (int i = off; i < off + len; i++) {
            int b = (int) ba[i];
            char c = digits.charAt((b >> 4) & 0xf);
            sb.append(c);
            c = digits.charAt(b & 0xf);
            sb.append(c);
	    switch(f.dataFormat){
	    case HEX_SPACE_HEX_FORMAT:
		sb.append(' ');
		break;
	    case HEX_COLON_HEX_FORMAT:
		sb.append(':');
		break;
	    case HEX_VOID_HEX_FORMAT:
	    default:
		break;
	    }
	}
	if (f.printAscii){
	    sb.append(' ');
	    for (int i = off; i < off + len; i++) {
		char c = (char) ba[i];
		if (Character.isLetterOrDigit(c)){
		    sb.append(c);
		}else{
		    sb.append('X');
		}
	    }
	}
	return sb.toString();
    }

    static class Format{
	/**
	 * Format of data printed.
	 */
	public int dataFormat;
	/**
	 * Prepend string with APDU length.
	 */
	public boolean printLength;
	/**
	 * Append ASCII representation of APDU.
	 */
	public boolean printAscii;
	/**
	 * Constructor.
	 */
	public Format(){
	    dataFormat = HEX_VOID_HEX_FORMAT;
	    printLength = false;
	    printAscii = false;
	}
	/**
	 * Constructor.
	 */
	public Format(int dataFormat, boolean printLength, boolean printAscii){
	    this.dataFormat = dataFormat;
	    this.printLength = printLength;
	    this.printAscii = printAscii;
	}
    }

}
