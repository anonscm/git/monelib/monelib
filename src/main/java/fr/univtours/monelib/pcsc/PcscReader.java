/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.monelib.pcsc;

import fr.univtours.monelib.Exceptions.PCSCException;
import java.util.ArrayList;
import java.util.List;
import javax.smartcardio.CardException;
import javax.smartcardio.CardTerminal;
import javax.smartcardio.CardTerminals;
import javax.smartcardio.TerminalFactory;

/**
 * Cette classe contient les méthodes statiques pour accéder à la liste des lecteurs PCSC
 * @author geoffroy.vibrac
 */
public abstract class PcscReader {


    private PcscReader(){
        
    }

    /**
     * Retourne la liste des noms des lecteurs PCSC disponibles
     * @return une List de chaines
     */
    public static List<String> getPCSCReaders() throws PCSCException {

        List<String> readerString = null;
        TerminalFactory factory = TerminalFactory.getDefault();
        CardTerminals terminalList = factory.terminals();
        
        try {
            List<CardTerminal> readers = terminalList.list();
            if (readers.size() > 0 ){
                readerString = new ArrayList<String>();
                for(CardTerminal reader : readers){
                    readerString.add(reader.getName());
                }
            }

        } catch (CardException ex) {
            throw new PCSCException(ex.getMessage());
        }

        return readerString;
    }

     /**
     * Retourne l'objet CardTerminal correspondant au lecteur indiqué en paramètres
     * utilisé par Connectioncard pour se connecté à la carte
     *
     * @return un objet CardTerminal representant le lecteur
     */

    static CardTerminal getCardTerminal(String ReaderName){
        CardTerminals terminalList;
        TerminalFactory factory = TerminalFactory.getDefault();
        terminalList = factory.terminals();
        return terminalList.getTerminal(ReaderName);

    }


}
