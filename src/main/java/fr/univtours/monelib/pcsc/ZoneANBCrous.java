/*
Copyright or © or Copr. Université de Tours
contributor(s) : Geoffroy VIBRAC (2011/07/01)

geoffroy.vibrac@gmail.com

This software is a computer program whose purpose is to [describe
functionalities and technical features of your software].

This software is governed by the CeCILL license under French law and
abiding by the rules of distribution of free software.  You can  use, 
modify and/ or redistribute the software under the terms of the CeCILL
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL license and that you accept its terms.
*/

package fr.univtours.monelib.pcsc;

import fr.univtours.monelib.Exceptions.PCSCException;





/**
 *
 * @author geoffroy.vibrac
 */
abstract class ZoneANBCrous extends ZoneANB implements CardZone {

    

        public ZoneANBCrous(ConnectionCard connCard, int ZoneAnbCrousRecherchee) throws PCSCException {

            super(connCard);
            byte[] zoneData = null;

            for (int i=0; i<10;i++){
                zoneData = getZoneContent(i);
                if (compareIdCrous(zoneData, ZoneAnbCrousRecherchee)){
                    this.zoneDump = zoneData;
                    break;
                }
            }
            if (zoneData == null){
                throw new PCSCException("Impossible de trouver la zone ANB " + ZoneAnbCrousRecherchee);
            }
        }
   
    final boolean compareIdCrous(byte[] contentZone, int numeroZoneCrous){

        // pour une zone crous, doit être = 0010
        String identifiantCnous="";
        // 1 = zone principale  - 2 = zone complementaire
        String identifiantBloc="";
        boolean returnValue = false;

        String contentZoneS = Util.ba2s(contentZone);

        if (contentZoneS!= null && !"".equals(contentZoneS) && contentZoneS.length() > 120){
            identifiantCnous = contentZoneS.substring(0, 4);
            identifiantBloc = contentZoneS.substring(8, 10);
            try{
                int identifiantBlocI = Integer.parseInt(identifiantBloc);
                if ("0010".equals(identifiantCnous) && identifiantBlocI == numeroZoneCrous){
                    returnValue = true;
                }
            }catch(ClassCastException e){  
            }catch(NumberFormatException e){                
            }
        }


        return returnValue;
    }


    int dateFormat(byte bOctet1, byte bOctet2){

            String octet1 = Util.getHexString(bOctet1);
            String octet2 = Util.getHexString(bOctet2);

            int annee = (Integer.parseInt(octet1,16) >> 1) + 2000;
            int mois = ((Integer.parseInt(octet1,16) & 0x01) << 3) | ((Integer.parseInt(octet2,16) & 0xe0) >> 5);
            int jour = (Integer.parseInt(octet2,16) & 0x1f);

            String laDate = annee + "" + mois + "" + jour;

            return Integer.parseInt(laDate);
    }

    abstract void constuctCard()  throws PCSCException;
    
    public abstract void printZone();

}
